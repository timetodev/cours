<?php
/* Divers code php pour l'utilisation colléctive.  */





/* Ouverture de balise php */
    <?php   ?>

/* Dire à php de dire bonjour */
    echo 'Bonjour!!!';


/* déclarais un variable à php */

    $bonjour = 'Bonjour';

/* dire à php d'utiliser ma variable */
    echo $bonjour;

/* Faire un tableau en php */
    $tableau = array('0','1','2','3');

/* dire à php d'afficher la premire valeur en php la valeur et 0 on doit donc appeller 0 car en php le tableau commence toujours à 0 */
    echo $tableau[0];

/* Faire des condition en php if, else if, else 


== (Est égal à)

>    (Est supérieur à)

< (Est inférieur à)

>=     (Est supérieur ou égal à)

<=  (Est inférieur ou égal à)

!= (Est différent de)

*/
    if($tableau[0] == 0)
    {
        echo 'Bonjour';
    }
    else if($tableau[0] == 1)
    {
        echo 'Indéterminé';
    }
    else
    {
        echo 'Aurevoir';
    }



?>